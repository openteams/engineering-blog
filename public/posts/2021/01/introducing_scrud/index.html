
<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Introducing SCRUD &#8212; OpenTeams Engineering  documentation</title>
    <link rel="stylesheet" href="../../../../_static/pygments.css" type="text/css" />
    <link rel="stylesheet" href="../../../../_static/alabaster.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../../../../_static/togglebutton.css" />
    <link rel="stylesheet" type="text/css" href="../../../../_static/mystnb.css" />
    <script id="documentation_options" data-url_root="../../../../" src="../../../../_static/documentation_options.js"></script>
    <script src="../../../../_static/jquery.js"></script>
    <script src="../../../../_static/underscore.js"></script>
    <script src="../../../../_static/doctools.js"></script>
    <script src="../../../../_static/togglebutton.js"></script>
    <script >var togglebuttonSelector = '.toggle, .admonition.dropdown, .tag_hide_input div.cell_input, .tag_hide-input div.cell_input, .tag_hide_output div.cell_output, .tag_hide-output div.cell_output, .tag_hide_cell.cell, .tag_hide-cell.cell';</script>
    <link rel="author" title="About these documents" href="../../../../about/" />
    <link rel="index" title="Index" href="../../../../genindex/" />
    <link rel="search" title="Search" href="../../../../search/" /> 
   
  <link rel="stylesheet" href="../../../../_static/custom.css" type="text/css" />
  
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />
  
<style type="text/css">
  ul.ablog-archive {
    list-style: none;
    overflow: auto;
    margin-left: 0px;
  }
  ul.ablog-archive li {
    float: left;
    margin-right: 5px;
    font-size: 80%;
  }
  ul.postlist a {
    font-style: italic;
  }
  ul.postlist-style-disc {
    list-style-type: disc;
  }
  ul.postlist-style-none {
    list-style-type: none;
  }
  ul.postlist-style-circle {
    list-style-type: circle;
  }
</style>

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          

          <div class="body" role="main">
             <div class="section" id="introducing-scrud">
<h1>Introducing SCRUD<a class="headerlink" href="#introducing-scrud" title="Permalink to this headline">¶</a></h1>
<div class="section" id="tldr-scrud-is-semantics-for-crud-through-rest">
<h2>TLDR; SCRUD is semantics for CRUD through REST.<a class="headerlink" href="#tldr-scrud-is-semantics-for-crud-through-rest" title="Permalink to this headline">¶</a></h2>
<p><a class="reference external" href="https://gitlab.com/openteams/semantic-http-spec">SCRUD</a> is an approach to defining a <a class="reference external" href="https://www.ics.uci.edu/~fielding/pubs/dissertation/rest_arch_style.htm">REST API</a> that consistently provides
discoverable schema and semantic meaning for your REST API resources. SCRUD stands for:
Semantics for Create, Retrieve, Update, and Delete. Before we get into how SCRUD
consistently provides discoverable schema and semantic meaning for REST APIs let’s talk
about why.</p>
</div>
<div class="section" id="why">
<h2>Why<a class="headerlink" href="#why" title="Permalink to this headline">¶</a></h2>
<p>SCRUD is motivated by observations about common approaches to REST API design.  For
example, <a class="reference external" href="https://json-schema.org/">JSON Schema</a> is great but it doesn’t tell us what the data in a
JSON resource mean. That’s not entirely fair since documentation in your schema can
tell a human what your data means, but text documentation in your schema it isn’t usable
by other software! You can’t use documentation to drive automated UI choices, better
indexing, etc..</p>
<p>Here are some additional observations:</p>
<ul class="simple">
<li><p>Schema can give us tools for free, but those tools are limited to well-formedness
due to a lack of associated meaning.</p></li>
<li><p>REST is great, but, it isn’t descriptive enough when it comes to what is allowed.
For example, if a URL supports POST of JSON resources, what is the schema of the
POST content and what does the posted data mean?</p></li>
<li><p><a class="reference external" href="http://spec.openapis.org/oas/v3.0.3">OpenAPI</a> is great and helps solve some of the REST API documentation
problem, but encourages a focus on URL patterns over hyptertextual resorces, making
REST APIs unnecessarily difficult to consume. <strong>Consuming a REST API should be
driven by following the links through the application of  HTTP verbs</strong>.</p></li>
<li><p>OpenAPI tells the client about the entire API, but doesn’t tell the client about
any specific resource URL. This means that in order to discover what’s supported by
a given URL you have to consult the OpenAPI specification for the API - sorta like
having to walk to a different table in a different room to use the tool that was
right in front of you, and you have to know that the other table exists and where
it’s located.</p></li>
</ul>
<p>All of these observations can lead one to a conclusion: our REST APIs could be better
documented, easier to consume, and software clients and libararies could do a lot more
heavy lifting for developers and users if we improved the discoverability of the schema,
semantics, and HTTP OPTIONS for all URLs.</p>
</div>
<div class="section" id="how">
<h2>How<a class="headerlink" href="#how" title="Permalink to this headline">¶</a></h2>
<p>All of the observations made so far have been made by others in a variety of settings.
Those observations have led to a variety of solutions and standards that address one or
two of the issues at at time. SCRUD chooses among the established standards and weaves
them together with the goal of a cohesive whole.</p>
<p>The existing standards SCRUD leverages include <a class="reference external" href="https://tools.ietf.org/html/rfc2616">HTTP</a>, <a class="reference external" href="https://tools.ietf.org/html/rfc8288">HTTP Link Header</a>, <a class="reference external" href="https://json-schema.org/">JSON Schema</a>, <a class="reference external" href="https://json-ld.org/">JSON-LD</a>, and an augmented
<a class="reference external" href="http://spec.openapis.org/oas/v3.0.3">OpenAPI</a> standard. SCRUD encourages defining JSON Schema and JSON-LD contexts
for all request and response entities and providing them in response headers. SCRUD also
defines an HTTP OPTIONS entity response format by borrowing from OpenAPI and encouraging
the mention of JSON Schema and JSON-LD contexts</p>
</div>
<div class="section" id="what">
<h2>What<a class="headerlink" href="#what" title="Permalink to this headline">¶</a></h2>
<p>SCRUD is a <a class="reference external" href="https://gitlab.com/openteams/semantic-http-spec">draft standard</a> defining basic expectations of a SCRUDful REST API.
For a JSON REST API these expectations include:</p>
<ul class="simple">
<li><p>A link header containing hyperlinks to a JSON Schema and JSON-LD context for all
HTTP responses that include an entity body.</p></li>
<li><p>Support for OPTIONS for all URLs that provide response entities with OpenAPI HTTP
Method content that provide links to the JSON Schema and JSON-LD Context for all
entity request and response bodies.</p></li>
</ul>
<div class="section" id="examples">
<h3>Examples<a class="headerlink" href="#examples" title="Permalink to this headline">¶</a></h3>
<div class="section" id="head">
<h4>HEAD<a class="headerlink" href="#head" title="Permalink to this headline">¶</a></h4>
<div class="section" id="request">
<h5>Request<a class="headerlink" href="#request" title="Permalink to this headline">¶</a></h5>
<div class="highlight-http notranslate"><div class="highlight"><pre><span></span><span class="nf">HEAD</span> <span class="nn">/api/partner-profiles/</span> <span class="kr">HTTP</span><span class="o">/</span><span class="m">1.1</span>
<span class="na">Host</span><span class="o">:</span> <span class="l">openteams.com</span>
<span class="na">Accept</span><span class="o">:</span> <span class="l">application/json</span>
</pre></div>
</div>
</div>
<div class="section" id="response">
<h5>Response<a class="headerlink" href="#response" title="Permalink to this headline">¶</a></h5>
<div class="highlight-http notranslate"><div class="highlight"><pre><span></span><span class="kr">HTTP</span><span class="o">/</span><span class="m">1.1</span> <span class="m">200</span> <span class="ne">OK</span>
<span class="na">Content-Type</span><span class="o">:</span> <span class="l">application/json</span>
<span class="na">Last-Modified</span><span class="o">:</span> <span class="l">Wed, 20 Jan 2021 17:00:00 GMT</span>
<span class="na">ETag</span><span class="o">:</span> <span class="l">&quot;XXXXX&quot;</span>
<span class="na">Link</span><span class="o">:</span> <span class="l">&lt;http://api.openteams.com/collections-json-schema/partner-profiles&gt;; rel=&quot;describedBy&quot;,</span>
      <span class="l">&lt;http://api.openteams.com/collections-json-ld/partner-profiles&gt;; rel=&quot;http://www.w3.org/ns/json-ld#context&quot;; type=&quot;application/ld+json&quot;</span>
</pre></div>
</div>
</div>
</div>
<div class="section" id="options">
<h4>OPTIONS<a class="headerlink" href="#options" title="Permalink to this headline">¶</a></h4>
<div class="section" id="id1">
<h5>Request<a class="headerlink" href="#id1" title="Permalink to this headline">¶</a></h5>
<div class="highlight-http notranslate"><div class="highlight"><pre><span></span><span class="nf">OPTIONS</span> <span class="nn">/api/partner-profiles/</span> <span class="kr">HTTP</span><span class="o">/</span><span class="m">1.1</span>
<span class="na">Host</span><span class="o">:</span> <span class="l">openteams.com</span>
<span class="na">Accept</span><span class="o">:</span> <span class="l">application/json</span>
</pre></div>
</div>
</div>
<div class="section" id="id2">
<h5>Response<a class="headerlink" href="#id2" title="Permalink to this headline">¶</a></h5>
<div class="highlight-http notranslate"><div class="highlight"><pre><span></span><span class="kr">HTTP</span><span class="o">/</span><span class="m">1.1</span> <span class="m">200</span> <span class="ne">OK</span>
<span class="na">Content-Type</span><span class="o">:</span> <span class="l">application/json</span>
<span class="na">Last-Modified</span><span class="o">:</span> <span class="l">Wed, 20 Jan 2021 17:00:00 GMT</span>
<span class="na">ETag</span><span class="o">:</span> <span class="l">&quot;XXXXX&quot;</span>
<span class="na">Link</span><span class="o">:</span> <span class="l">&lt;http://api.openteams.com/json-schema/HTTPSchema&gt;; rel=&quot;describedBy&quot;,</span>
      <span class="l">&lt;http://api.openteams.com/json-ld/HTTPSchema&gt;; rel=&quot;http://www.w3.org/ns/json-ld#context&quot;; type=&quot;application/ld+json&quot;</span>

<span class="p">{</span>
  <span class="nt">&quot;get&quot;</span><span class="p">:</span> <span class="p">{</span>
    <span class="nt">&quot;responses&quot;</span><span class="p">:</span> <span class="p">{</span>
      <span class="nt">&quot;200&quot;</span><span class="p">:</span> <span class="p">{</span>
        <span class="nt">&quot;description&quot;</span><span class="p">:</span> <span class="s2">&quot;OK&quot;</span><span class="p">,</span>
        <span class="nt">&quot;content&quot;</span><span class="p">:</span> <span class="p">{</span>
          <span class="nt">&quot;application/json&quot;</span><span class="p">:</span> <span class="p">{</span>
            <span class="nt">&quot;schema&quot;</span><span class="p">:</span>
              <span class="s2">&quot;http://api.openteams.com/collections-json-schema/partner-profiles&quot;</span><span class="p">,</span>
            <span class="nt">&quot;context&quot;</span><span class="p">:</span>
              <span class="s2">&quot;http://api.openteams.com/collections-json-ld/partner-profiles&quot;</span>
          <span class="p">}</span>
        <span class="p">}</span>
      <span class="p">}</span>
    <span class="p">}</span>
  <span class="p">},</span>
  <span class="nt">&quot;post&quot;</span><span class="p">:</span> <span class="p">{</span>
    <span class="nt">&quot;requestBody&quot;</span><span class="p">:</span> <span class="p">{</span>
      <span class="nt">&quot;description&quot;</span><span class="p">:</span>
        <span class="s2">&quot;Create a new http://api.openteams.com/json-ld/partner-profiles resource.&quot;</span><span class="p">,</span>
      <span class="nt">&quot;required&quot;</span><span class="p">:</span> <span class="kc">true</span><span class="p">,</span>
      <span class="nt">&quot;content&quot;</span><span class="p">:</span> <span class="p">{</span>
        <span class="nt">&quot;schema&quot;</span><span class="p">:</span> <span class="s2">&quot;http://api.openteams.com/json-schema/partner-profiles&quot;</span><span class="p">,</span>
        <span class="nt">&quot;context&quot;</span><span class="p">:</span> <span class="s2">&quot;http://api.openteams.com/json-ld/partner-profiles&quot;</span>
      <span class="p">}</span>
    <span class="p">},</span>
    <span class="nt">&quot;responses&quot;</span><span class="p">:</span> <span class="p">{</span>
      <span class="nt">&quot;201&quot;</span><span class="p">:</span> <span class="p">{</span>
        <span class="nt">&quot;description&quot;</span><span class="p">:</span>
          <span class="s2">&quot;CREATED. A new http://api.openteams.com/json-ld/partner-profiles resource was</span>
<span class="s2">          successfully created.&quot;</span><span class="p">,</span>
        <span class="nt">&quot;headers&quot;</span><span class="p">:</span> <span class="p">{</span>
          <span class="nt">&quot;Location&quot;</span><span class="p">:</span> <span class="s2">&quot;The URL of the created resource.&quot;</span>
        <span class="p">}</span>
      <span class="p">}</span>
    <span class="p">}</span>
  <span class="p">}</span>
<span class="p">}</span>
</pre></div>
</div>
<p>This combination of always providing the link headers and providing an enriched version
of HTTP OPTIONS support make the supported operations for an individual REST resource
discoverable. What’s more, it lays the foundation for a richly defaulted and extensible
browsing client library.</p>
<p>An early draft of the standard can be found
<a class="reference external" href="https://gitlab.com/openteams/semantic-http-spec">here</a>. Our intent at OpenTeams is to
submit an RFC to the IETF sometime later this year.</p>
<p>OpenTeams has defined and begun adoption of SCRUD to better document our APIs and to
enable our small team of developers to move faster by taking advantage of the
information provided by SCRUD APIs. We have two primary SCRUD open source projects to
date: scrud-django for implenting REST APIs and scrud-nuxt to support our front-end
development. I’ll be introducing these projects, soon!</p>
</div>
</div>
</div>
</div>
<div class="section" id="summary">
<h2>Summary<a class="headerlink" href="#summary" title="Permalink to this headline">¶</a></h2>
<p>SCRUD aims to add meaning and enriched discoverability to REST APIs to better support
both the humans who write applications and for the tools that help them write
applications. SCRUD accomplishes these goals by combining and adapting existing
standards, including HTTP, HTTP Link Headers, JSON Schema, JSON-LD, and OpenAPI. At
OpenTeams we’ve started two open source projects to build future OpenTeams capability
on: scrud-django and scrud-nuxt.</p>
</div>
</div>

<div class="section">
       
</div>

          </div>
          
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<h1 class="logo"><a href="../../../../">OpenTeams Engineering</a></h1>








  
<h2>
    20 January 2021  
</h2>

<ul>
   
<li id="author">
  <span
    >Author:</span
  >
   David Charboneau 
</li>
   
<li id="category">
  <span
    >Category:</span
  >
   Introduction 
</li>
 
<li id="tags">
  <span
    >Tags: </span
  >
   scrud  rest 
</li>
 
</ul>
<h3>Navigation</h3>
<ul>
<li class="toctree-l1"><a class="reference internal" href="../../../../about/">About OpenTeams Engineering</a></li>
</ul>


<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="../../../../search/" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2021, OpenTeams, Inc..
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 3.4.3</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.12</a>
      
      |
      <a href="../../../../_sources/posts/2021/01/introducing_scrud.md.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>